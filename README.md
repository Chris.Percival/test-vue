# Package.json

## moduleFileExtensions

Tells Jest which extensions to look for

## transform

Which preprocessor to use for a file extension

# Shallow Rendering

Shallow Rendering is a technique that assures your component is rendering without children. This is useful for:

    > Testing only the component you want to test (that's what Unit Test stands for)

    > Avoid side effects that children components can have, such as making HTTP calls, calling store actions...
